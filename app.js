/*jslint node: true */
/*jslint esnext: true */
"use strict";

var apiConfig = {
  airbnbUrl: 'https://m.airbnb.ca/api/-/v1/listings/search',
  airbnbUrl2: 'https://api.airbnb.com/v1/listings/search',
  airbnbUrlv2: 'https://www.airbnb.com/api/v2/search_results',
  airbnbKey: 'd306zoyjsyarp7ifhu67rjxn52tv0t20',
  baseAirUrl: 'https://c31.travelpayouts.com/click?shmarker=81251&promo_id=645&source_type=customlink&type=click&custom_url='
};

var express = require('express');
var bodyParser = require('body-parser');
var request = require('request');

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/air', function(req, res) {
  request({
    url: apiConfig.airbnbUrlv2,
    json: true,
    qs: req.query
  }, function(error, response, body) {
    if (!error && response.statusCode === 200) {
      res.jsonp(body);
    } else {
      var stautsCode = (response && response.statusCode) || 500;
      res.status(stautsCode).jsonp({
        body: body,
        statusCode: stautsCode
      });
    }
  });
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;

  console.log("Warning - [404] " + req.originalUrl + " from " + req.get('user-agent'));
  next(err);
});

// production/dev error handler
// no stacktraces leaked to user unless dev
app.use(function(err, req, res, next) {
  res.status(err.status).send(err.status);
});


module.exports = app;
